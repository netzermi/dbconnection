import java.util.Optional;

public class OptExamples {

    public static void main(String[] args) {
        Optional obj1 = Optional.of
                ("This is a sample text");
        Optional obj2 = Optional.empty();

        Optional opt = obj1;
        opt.ifPresent(System.out::println);


    }
}
