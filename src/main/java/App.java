import java.sql.*;
import java.util.List;

import at.hakimst.apr.db.CustomerDAO;
import at.hakimst.apr.db.DBConnection;
import at.hakimst.apr.model.Customer;
public class App {

    public static void main(String[] args) {
        CustomerDAO customerDAO = new CustomerDAO();
        List<Customer> customers = customerDAO.getAll();
        System.out.println(customers);


    }

    public static void insertExample(Connection connection) throws SQLException{
        Statement statement = connection.createStatement();
        String sqlInsert = "INSERT INTO `publisher`(`publisher_name`) VALUES ('NEMI')";
        statement.execute(sqlInsert);
    }

    public static void insertExamplePrepared(Connection connection) throws SQLException{
        String sqlInsertPrepare = "INSERT INTO publisher (publisher_name) VALUES (?)";
        PreparedStatement statement = connection.prepareStatement(sqlInsertPrepare);
        statement.setString(1, "NEMI IS PREPARED");
        statement.executeUpdate();
    }



    public static void printAllPublisher(Connection connection) throws SQLException {
        String sql = "SELECT * FROM publisher";
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(sql);
        while(resultSet.next()){
            System.out.println(resultSet.getString("publisher_name"));
        }
    }
}
