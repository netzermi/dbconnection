package at.hakimst.apr.db;

import at.hakimst.apr.model.Customer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CustomerDAO implements IDAO<Customer> {


    @Override
    public Customer get(long id) {
        Connection connection = DBConnection.getConnection();
        Customer c = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM customer WHERE id=?");
            preparedStatement.setLong(1, id);
            ResultSet rs = preparedStatement.executeQuery();
            if(rs.next()){
                String idString = rs.getString("id");
                String firstname = rs.getString("firstname");
                String lastname = rs.getString("lastname");
                c = new Customer(idString, firstname, lastname);
            } else {
                return null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return c;
    }

    @Override
    public List<Customer> getAll() {
        List<Customer> customerList = new ArrayList<>();
        Connection connection = DBConnection.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM customer");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                String id = resultSet.getString("id");
                String firstName = resultSet.getString("firstname");
                String lastName = resultSet.getString("lastname");
                Customer c = new Customer(id, firstName, lastName);
                customerList.add(c);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return  customerList;
    }

    @Override
    public void save(Customer k) {
        Connection connection = DBConnection.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO customer(firstname, lastname) VALUES (?, ?)", Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, k.getFirstname());
            preparedStatement.setString(2, k.getLastname());
            preparedStatement.executeUpdate();
            ResultSet rs = preparedStatement.getGeneratedKeys();
            if (rs.next()) {
                    String dbID = rs.getString(1);
                    k.setId(dbID);
            }
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(Customer k) {
        Connection connection = DBConnection.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("UPDATE `customer` SET firstname=?,lastname=? WHERE id = ?");
            preparedStatement.setString(3,k.getId());
            preparedStatement.setString(1,k.getFirstname());
            preparedStatement.setString(2,k.getLastname());
            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void delete(Customer k) {
        Connection connection = DBConnection.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM customer WHERE id=?");
            preparedStatement.setString(1, k.getId());
            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
