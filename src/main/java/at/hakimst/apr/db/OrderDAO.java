package at.hakimst.apr.db;

import at.hakimst.apr.model.Customer;
import at.hakimst.apr.model.Order;

import java.sql.*;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

public class OrderDAO implements IDAO<Order>{

    @Override
    public Order get(long id) {
        return null;
    }

    @Override
    public List<Order> getAll() {
        List<Order> orderList = new ArrayList<>();
        Connection connection = DBConnection.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM order");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                String id = resultSet.getString("id");
                Date sqlDate = resultSet.getDate("date");
                GregorianCalendar cal = new GregorianCalendar();
                cal.setTime(sqlDate);
                String desc = resultSet.getString("description");
                Order order = new Order(Integer.valueOf(id),cal, desc);
                orderList.add(order);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return  orderList;



    }

    @Override
    public void save(Order o) {

    }

    @Override
    public void update(Order o) {

    }

    @Override
    public void delete(Order o) {

    }

    public static void main(String[] args) {
        //from sql date to greg cal
        Date sqlDate = Date.valueOf("2023-03-27");
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(sqlDate);
        System.out.println(cal.getTime());
        //from greg cal to sql date
        Date sqlDate2 = new Date(cal.getTimeInMillis());
        System.out.println(sqlDate2);


    }
}
