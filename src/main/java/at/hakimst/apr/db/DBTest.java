package at.hakimst.apr.db;

import java.sql.*;
import java.util.Scanner;

public class DBTest {
    private static Connection conn = DBConnection.getConnection();


    public static void main(String[] args) {
        //createExample();
        //deleteExample();
        menu();
    }

    public static void menu(){
        while(true){
            System.out.println("Your choice: 1) add 2) read 3) update 4) delete 5) show all q) quit");
            Scanner scanner = new Scanner(System.in);
            String choice = scanner.nextLine();
            switch(choice){
                case "1":
                    System.out.println("Enter name in form: firstname lastname");
                    String name = scanner.nextLine();
                    create(name.split(" ")[0], name.split(" ")[1]);
                    break;
                case "q": System.exit(0);
                default: break;
            }

        }
    }

    public static void create(String firstName, String lastName){
       String sql = "INSERT INTO customer (firstname, lastname) VALUES (?,?)";
        try {
            PreparedStatement s = conn.prepareStatement(sql);
            s.setString(1, firstName);
            s.setString(2, lastName);
            s.executeUpdate();
        } catch(SQLException e){
            e.printStackTrace();
        }
    }

    public static void readExample(){
        int id = 1;
        try {

            String sql = "SELECT * FROM customer WHERE id=?";
            PreparedStatement s = conn.prepareStatement(sql);
            s.setInt(1, id);
            ResultSet result = s.executeQuery();
            while(result.next()){
                System.out.println(result.getString(2) + " " + result.getString(3));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void readAll(){
        try {

            String sql = "SELECT * FROM customer";
            PreparedStatement s = conn.prepareStatement(sql);
            ResultSet result = s.executeQuery();
            while(result.next()){
                System.out.println(result.getString(1) + " " +  result.getString(2) + " " + result.getString(3));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void deleteExample(){
        //TODO: Entferne Datensatz mit ID 1
        int id = 1;
        String sql = "DELETE FROM customer WHERE id=?";
        try {
            PreparedStatement s = conn.prepareStatement(sql);
            s.setInt(1, id);
            s.executeUpdate();
        } catch(SQLException e){
            e.printStackTrace();
        }
    }

    public static void updateExample(){
        //TODO: Aktualisiere einen Datensatz
        int id = 3;
        String sql = "UPDATE customer SET firstname=?, lastname=? WHERE ID=?";
        try {
            PreparedStatement s = conn.prepareStatement(sql);
            s.setString(1, "Stefan");
            s.setString(2, "Netzer");
            s.setInt(3, id);
            s.executeUpdate();
        } catch(SQLException e){
            e.printStackTrace();
        }
    }
}
