package at.hakimst.apr.db;

import at.hakimst.apr.model.Customer;

import java.util.List;

public interface IDAO<T> {

    public T get(long id);
    public List<T> getAll();

    public void save(T o);

    public void update(T o);

    public void delete(T o);

}
