package at.hakimst.apr.model;

public class Customer {
    private String id;
    private String firstname;
    private String lastname;

    public Customer(String id, String firstname, String lastname) {
           this.id = id;
           this.firstname = firstname;
           this.lastname = lastname;
    }

    public String toString() {
        return id + " - " + firstname + " "+ lastname;
    }

    public String getId() {
        return id;
    }

    public void setId(String id){
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }
}
