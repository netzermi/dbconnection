package at.hakimst.apr.model;

import java.util.GregorianCalendar;

public class Order {
    private int number;
    private GregorianCalendar date;

    private String description;

    public Order(int number, GregorianCalendar date, String description) {
        this.number = number;
        this.date = date;
        this.description = description;
    }
}
