package at.hakimst.apr.jpa;

import at.hakimst.apr.db.IDAO;
import at.hakimst.apr.model.Customer;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;

public class JPACustomerDAO implements IDAO<Customer> {
    private EntityManager entityManager;
    @Override
    public Customer get(long id) {
       Optional<Customer> res =  Optional.ofNullable(entityManager.find(Customer.class, id));
        return res.get();
    }

    @Override
    public List<Customer> getAll() {
        return null;
    }

    @Override
    public void save(Customer o) {

    }

    @Override
    public void update(Customer o) {

    }

    @Override
    public void delete(Customer o) {

    }
}
